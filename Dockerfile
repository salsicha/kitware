FROM ubuntu:bionic

# https://gitlab.kitware.com/keu-computervision/slam
# https://gitlab.com/salsicha/kitware/

# docker build -t paraview .
# docker run -ti --network host paraview

ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    ROS_DISTRO=melodic \
    UBUNTU_DISTRO=bionic \
    DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y gnupg2

RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $UBUNTU_DISTRO main" > /etc/apt/sources.list.d/ros-latest.list'
RUN apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

RUN apt update && apt install -y ros-$ROS_DISTRO-ros-base

RUN apt install -y paraview vim git build-essential cmake libgtest-dev libeigen3-dev 
RUN apt install -y libceres-dev ros-$ROS_DISTRO-libg2o paraview-dev libcpl-dev ros-$ROS_DISTRO-pcl-ros
RUN apt install -y ros-$ROS_DISTRO-gps-common ros-$ROS_DISTRO-geodesy 
RUN apt install -y libopenni-dev libopenni2-dev libusb-dev libusb-1.0-0-dev libusb-1.0-0
RUN apt install -y wget ninja-build build-essential python3-rosdep python3-wstool ros-$ROS_DISTRO-diagnostics
RUN apt install -y libpcap-dev libpcap0.8-dev libpcap0.8 libpng-dev libpng-tools libpng16-16 libpng-dev

RUN apt install -y ros-$ROS_DISTRO-roslint ros-$ROS_DISTRO-tf2-sensor-msgs ros-$ROS_DISTRO-tf2-geometry-msgs
RUN apt install -y ros-melodic-angles libyaml-cpp-dev libpython3.7 liblas3

RUN ln -s /usr/lib/x86_64-linux-gnu/libpcap.so.1.8.1 /usr/lib/x86_64-linux-gnu/libpcap.so.1

RUN cd /root/ && git clone https://github.com/jlblancoc/nanoflann.git
WORKDIR /root/nanoflann
RUN mkdir build && cd build && cmake ..
RUN cd build && make -j8 && make install

RUN cd /root/ && git clone https://github.com/RainerKuemmerle/g2o.git
WORKDIR /root/g2o
RUN mkdir build
RUN cd build && cmake ..
RUN cd build && make -j8 && make install

RUN echo "source /opt/ros/$ROS_DISTRO/setup.bash" >> ~/.bashrc
RUN echo "source /root/catkin_ws/devel/setup.bash" >> ~/.bashrc

RUN /bin/bash -c "source /opt/ros/$ROS_DISTRO/setup.bash && rosdep init && rosdep update --rosdistro $ROS_DISTRO"
RUN mkdir -p ~/catkin_ws/src
WORKDIR /root/catkin_ws/src
RUN git clone https://gitlab.com/salsicha/kitware.git
RUN git clone https://github.com/ros-drivers/velodyne.git
RUN /bin/bash -c "source /opt/ros/$ROS_DISTRO/setup.bash && cd ~/catkin_ws/ && catkin_make -j8 -DCMAKE_BUILD_TYPE=Release"

### Usage:
# roslaunch lidar_slam slam.launch

# rviz -d ros_wrapping/lidar_slam/params/slam.rviz


### Now install on host where Paraview will run:

### update cmake to 3.12
# sudo apt-get update
# sudo apt-get install apt-transport-https ca-certificates gnupg software-properties-common wget
# wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | sudo apt-key add -
# sudo apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main'
# sudo apt-get update
# sudo apt-get install kitware-archive-keyring
# sudo apt-key --keyring /etc/apt/trusted.gpg del C1F34CDD40CD72DA
# sudo apt-get install cmake-data=3.16.5-0kitware1 cmake=3.16.5-0kitware1

# QT5=5.12 ...

### Install Paraview:
# sudo apt-get install git cmake build-essential libgl1-mesa-dev libxt-dev qt5-default libqt5x11extras5-dev libqt5help5 qttools5-dev qtxmlpatterns5-dev-tools libqt5svg5-dev python3-dev python3-numpy libopenmpi-dev libtbb-dev ninja-build
# git clone --recursive https://gitlab.com/salsicha/paraview.git
# mkdir paraview_build
# cd paraview_build
# cmake -GNinja -DPARAVIEW_USE_PYTHON=ON -DPARAVIEW_USE_MPI=ON -DVTK_SMP_IMPLEMENTATION_TYPE=TBB -DCMAKE_BUILD_TYPE=Release ../paraview
# ninja
# ninja install

### Build from source:
# RUN cd /root && git clone https://gitlab.com/salsicha/kitware.git
# WORKDIR /root/kitware
# RUN mkdir build
# RUN cd build && cmake .. -DCMAKE_BUILD_TYPE=RelWithDebInfo -DSLAM_PARAVIEW_PLUGIN:BOOL=ON
# RUN cd build && make -j8 && make install

### Prebuilt (requires libboost 1.66, which I can't find...):
# COPY LidarView-3.6.0-Linux-64bit-SLAM.tar.gz /root/LidarView-3.6.0-Linux-64bit-SLAM.tar.gzZ

### Usage:
# Open ParaView
# Tools tab > Manage Plugins > Load New
# Browse to the <install>/lib/ dir and select libLidarSlamPlugin.so
# Load LiDAR frames and LiDAR calibration to use
# Select the frames in the Pipeline Browser, instantiate a SLAM filter, and apply it.


CMD ["/bin/bash"]
